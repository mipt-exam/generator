package com.exam.generator.storage;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.function.Function;


@Getter
public class CurrencyValues {

    private final LocalDateTime moment;
    private final BigDecimal value;

    private CurrencyValues(LocalDateTime moment, BigDecimal value) {
        this.moment = moment;
        this.value = value;
    }

    public static CurrencyValues create(BigDecimal value) {
        return new CurrencyValues(LocalDateTime.now(), value);
    }
}
