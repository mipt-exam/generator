package com.exam.generator.storage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CurrencyStorage {

    private static final Logger logger = LogManager.getLogger(CurrencyStorage.class);

    private final Map<String, LinkedList<CurrencyValues>> storage = new HashMap<>();
    private final int capacity = 100;

    public LocalDateTime getLastMoment(String currency) {
        return storage.get(currency).getLast().getMoment();
    }

    public void currencyRegistration(String currency) {
        storage.put(currency, new LinkedList<>());
        logger.info(String.format("Валюта %s успешно зарегестрирована", currency));
    }

    private boolean isCurrencyRegistered(String currency) {
        return storage.containsKey(currency);
    }

    public void put(String currency, BigDecimal value) {
        if (!isCurrencyRegistered(currency)) {
            logger.warn(String.format("Значение не сохранено. Причина: валюта %s не зарегистрирована", currency));
            return;
        }
        updateList(getCurrencyStorage(currency), value);
    }

    private LinkedList<CurrencyValues> getCurrencyStorage(String currency) {
        return storage.get(currency);
    }

    public void updateList(LinkedList<CurrencyValues> list, BigDecimal value) {
        if (list.size() == capacity) {
            list.removeLast();
        }
        list.addFirst(CurrencyValues.create(value));
    }

    public List<String> getRegisteredCurrencies() {
        return new ArrayList<>(storage.keySet());
    }

    public List<CurrencyValues> getValues(String currency) {
        if (storage.get(currency) == null || storage.get(currency).size() == 0) {
            logger.warn(String.format("Валюта %s не зарегистрирована или не имеет значений", currency));
            return new LinkedList<>();
        }
        return new LinkedList<>(storage.get(currency));
    }

    public List<CurrencyValues> getValuesFromMoment(String currency, LocalDateTime from) {
        if (storage.get(currency) == null || storage.get(currency).size() == 0) {
            logger.warn(String.format("Валюта %s не зарегистрирована или не имеет значений", currency));
            return new LinkedList<>();
        }
        List<CurrencyValues> result = new LinkedList<>(storage.get(currency).stream()
                .filter(v -> v.getMoment().isAfter(from))
                .collect(Collectors.toList()));
        return result;
    }

    public boolean isCurrencyListEmpty(String currency) {
        return storage.get(currency).isEmpty();
    }

    public BigDecimal getLastValue(String currency) {
        return storage.get(currency).getLast().getValue();
    }
}