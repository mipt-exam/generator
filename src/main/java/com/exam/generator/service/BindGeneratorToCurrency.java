package com.exam.generator.service;

import com.exam.generator.service.generator.ValueGenerator;
import lombok.Getter;

@Getter
public class BindGeneratorToCurrency {
    private final String currencyName;
    private final ValueGenerator generator;

    private BindGeneratorToCurrency(String currencyName, ValueGenerator generator) {
        this.currencyName = currencyName;
        this.generator = generator;
    }

    public static BindGeneratorToCurrency create(String currency, ValueGenerator generator) {
        return new BindGeneratorToCurrency(currency, generator);
    }
}