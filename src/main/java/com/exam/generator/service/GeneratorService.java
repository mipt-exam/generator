package com.exam.generator.service;

import com.exam.generator.config.ConfigCurrencyValues;
import com.exam.generator.config.ConfigContainer;
import com.exam.generator.service.generator.GeneratorParams;
import com.exam.generator.service.generator.ValueGenerator;
import com.exam.generator.storage.CurrencyStorage;
import com.exam.generator.storage.CurrencyValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
public class GeneratorService {

    private static final Logger logger = LogManager.getLogger(GeneratorService.class);

    private final ConfigContainer config;
    private final CurrencyStorage storage;
    private final List<String> currencies = new LinkedList<>();
    private final LinkedList<BindGeneratorToCurrency> binds = new LinkedList<>();
    private boolean generationIsRunning = false;

    @Autowired
    public GeneratorService(ConfigContainer config,
                            CurrencyStorage storage) {
        this.config = config;
        this.storage = storage;

        createCurrencyPairsList();
        registrationCurrenciesInStorage();
        connectGeneratorAndCurrencies();
        start();

    }

    private void createCurrencyPairsList() {
        if (config.getCurrencyPairs().size() < 1) {
            throw  new RuntimeException("Список валют не может быть пустым");
        }
        for (int i = 0; i < config.getCurrencyPairs().size(); i++)
            currencies.add(config.getCurrencyPairs().get(i).getCurrencyName());
    }

    private void registrationCurrenciesInStorage() {
        for (String currency : currencies) {
            storage.currencyRegistration(currency);
        }
    }

    private void connectGeneratorAndCurrencies() {
        for (ConfigCurrencyValues c : config.getCurrencyPairs())
            createGenerator(c);
    }

    private void createGenerator(ConfigCurrencyValues values) {
        try {
            Class<?> generatorClass = Class.forName(values.getGenerator());
            ValueGenerator generator = (ValueGenerator) generatorClass.getDeclaredConstructor().newInstance();
            binds.add(BindGeneratorToCurrency.create(values.getCurrencyName(), generator));
        } catch (Exception e) {
            logger.warn("Не удалось связать валюту с генератором значений. Класс генератора не найден: " + e.getMessage());
        }
    }

    public void start() {

        if (generationIsRunning) {
            logger.warn("Генерация уже запущена");
            return;
        }

        Thread generationThread = new Thread(() -> {
            while (true) {
                for (BindGeneratorToCurrency bind : binds) {
                    try {
                        String currency = bind.getCurrencyName();
                        BigDecimal initValue = config.getCurrencyPairs().stream()
                                .filter(i -> bind.getCurrencyName().equals(i.getCurrencyName()))
                                .map(i -> new BigDecimal(i.getValue()))
                                .findFirst().orElseThrow(RuntimeException::new);

                        if (!storage.isCurrencyListEmpty(currency)) {
                            GeneratorParams params = new GeneratorParams(initValue,
                                                                         storage.getLastValue(currency),
                                                                         storage.getLastMoment(currency));
                            storage.put(currency, bind.getGenerator().generateValue(params));
                        } else {
                            storage.put(currency, initValue);
                        }
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        logger.error("Ошибка при генерации значения валюты: " + e.getMessage());
                    }
                }
            }
        });
        generationThread.start();
        generationIsRunning = true;
        logger.info("Генерация значений запущена" + LocalDateTime.now());
    }

    public Iterable<CurrencyValues> getValues(String currency) {
        return storage.getValues(currency);
    }

    public Iterable<CurrencyValues> getValuesFromMoment(String currency, LocalDateTime from) {
        return storage.getValuesFromMoment(currency, from);
    }

    public Iterable<String> getRegisteredCurrencies() {
        return storage.getRegisteredCurrencies();
    }

}
