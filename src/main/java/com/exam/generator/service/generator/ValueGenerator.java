package com.exam.generator.service.generator;

import java.math.BigDecimal;

public interface ValueGenerator {

    BigDecimal generateValue(GeneratorParams params);
}
