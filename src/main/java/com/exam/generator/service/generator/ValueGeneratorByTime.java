package com.exam.generator.service.generator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;


@Component
public class ValueGeneratorByTime implements ValueGenerator {

    /**
     * Генерирует новое значение цены валютной пары, используя в качестве основы предыдущую сгенерированную цену,
     * а также гарантирует невыход новой цены за границу максимального отклонения
     *
     * @param prevPair: .getInitValue() — возвращает базовое значение цены пары при запуске сервиса
     *                  .getLastValue() — последнее сгенерированное значение цены
     *                  .getLastMoment() — дату и время формата генерации последней цены в формате LocalDateTime
     * @return BigDecimal с новой сгенерированной ценой
     */

    @Override
    public BigDecimal generateValue(GeneratorParams prevPair) {

        double maxDeviationPercent = prevPair.getInitValue().doubleValue()
                / 100 * PairChangeSettings.percentOfDeviation * 2;

        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(prevPair.getLastMoment(), now);

        int passedSeconds = (int) duration.toSeconds();

        double dispersion = passedSeconds * 0.01 / PairChangeSettings.changePeriodInSeconds();
        dispersion = Math.round(dispersion * 100d) / 100d;

        if (dispersion > maxDeviationPercent) {
            dispersion = maxDeviationPercent;
        }

        double minEdge = prevPair.getLastValue().doubleValue() - dispersion / 2;
        double maxEdge = prevPair.getLastValue().doubleValue() + dispersion / 2;

        double difference = 0;
        if (minEdge < prevPair.getInitValue().doubleValue() - maxDeviationPercent / 2) {
            difference = (prevPair.getInitValue().doubleValue() - maxDeviationPercent / 2) - minEdge;
        }

        if (maxEdge > prevPair.getInitValue().doubleValue() + maxDeviationPercent / 2) {
            difference = (prevPair.getInitValue().doubleValue() + maxDeviationPercent / 2) - maxEdge;
        }

        double dice = (Math.random() * dispersion) - (dispersion / 2) + difference + 0.01;
        BigDecimal shift = BigDecimal.valueOf(dice).setScale(2, RoundingMode.DOWN);

        return prevPair.getLastValue().add(shift);
    }

    /**
     * Управляет протяжённостью периода, за который цена способна измениться по модулю на 0.01,
     * по умолчанию период изменения цены равен трёхстам тысячам миллисекунд или 5 минутам
     */
    private static class PairChangeSettings {
        static double percentOfDeviation = 40;
        static int changePeriodInMills = 1000;

        static int changePeriodInMinutes() {
            return changePeriodInMills / 1000 / 60;
        }

        static int changePeriodInSeconds() {
            return changePeriodInMills / 1000;
        }
    }
}