package com.exam.generator.service.generator;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class GeneratorParams {

    private final BigDecimal initValue;
    private final BigDecimal lastValue;
    private final LocalDateTime lastMoment;

    public GeneratorParams(BigDecimal initValue, BigDecimal lastValue, LocalDateTime lastMoment) {
        this.initValue = initValue;
        this.lastValue = lastValue;
        this.lastMoment = lastMoment;
    }

}
