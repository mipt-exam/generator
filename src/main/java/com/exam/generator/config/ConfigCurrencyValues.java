package com.exam.generator.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class ConfigCurrencyValues {

    private String currencyName;
    private String value;
    private String generator;

}
