package com.exam.generator.controller;

import com.exam.generator.service.GeneratorService;
import com.exam.generator.storage.CurrencyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class GeneratorController {

    private final GeneratorService service;

    @Autowired
    public GeneratorController(GeneratorService service) {
        this.service = service;
    }

    @GetMapping(value = "/")
    public String check() {
        return "Port is listened";
    }

    @GetMapping(value = "/currency/registered")
    public Iterable<String> getRegistretedCurrencies() {
        return service.getRegisteredCurrencies();
    }

    @GetMapping(value = "/currency/values/{pairName}")
    public Iterable<CurrencyValues> getCurrencyPairValues(@PathVariable String pairName) {
        return service.getValues(pairName);
    }

    @GetMapping(value = "/currency/values/criteria")
    public ResponseEntity getCurrencyPairValuesFromMoment(@RequestParam String pairName, @RequestParam String from) {
        LocalDateTime ldtFrom;
        try {
            ldtFrom = LocalDateTime.parse(from);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("Некорректный формат времени");
        }
        try {
            return ResponseEntity.ok(service.getValuesFromMoment(pairName, ldtFrom));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("Не удалось получить значение валюты");
        }
    }
}
