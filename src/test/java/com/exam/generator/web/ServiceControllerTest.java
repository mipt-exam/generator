package com.exam.generator.web;

import com.exam.generator.controller.GeneratorController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ServiceControllerTest {

    @Autowired
    private GeneratorController controller;

    @Test
    public void contextLoads() throws NullPointerException {
            assertThat(controller).isNotNull();
    }

}
