package com.exam.generator.web;

import com.exam.generator.exceptions.NotFoundException;
import com.exam.generator.service.GeneratorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.net.ConnectException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getDefaultMessage() throws ConnectException {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/v1/",
                String.class)).contains("Port is listened");
    }

    private final List<String> currencyList = List.of("USD_EUR", "USD_RUB", "USD_JPY");

    @Test
    public void getCurrencyList() throws NotFoundException{
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/v1/currency/registered",
                String.class)).contains(currencyList);

    }

}
