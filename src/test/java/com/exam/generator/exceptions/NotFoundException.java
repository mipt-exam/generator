package com.exam.generator.exceptions;

public class NotFoundException extends RuntimeException {

    private String request;

    public NotFoundException(String message, String request) {
        super(message);
        this.request = request;
    }

}
