FROM openjdk:11
COPY ./target/generator.jar /app/
WORKDIR /app/
EXPOSE 49080
CMD java -jar /app/generator.jar
